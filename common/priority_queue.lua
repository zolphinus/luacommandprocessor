------------
-- Locals -- 
------------

local PriorityQueue = {}
local PriorityQueue_mt = { __index = PriorityQueue }

-----------------
-- Constructor --
-----------------

function PriorityQueue.new(  )
	local PriorityQueue = {}

	return setmetatable( PriorityQueue, PriorityQueue_mt )
end

-------------
-- Methods -- 
-------------

function PriorityQueue:print()
	local string = "["
	for i=1, #self do
		string = string .. self[i]
		if (i ~= #self) then
			string = string .. ", "
		end
	end
	string = string .. "]"
	print( string )
end

function PriorityQueue:add(item)
  --refactor to do a priority comparison then insert into table
	self[#self + 1] = item
	return item
end

function PriorityQueue:remove()
	return table.remove( self, 1 )
end

function PriorityQueue:length(  )
	return #self
end

function PriorityQueue:clear( )
	for i=1, #self do
		self:pop()
	end
end

return PriorityQueue