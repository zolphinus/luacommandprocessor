local Queue = require( "common.queue" )
local print_r = require( "common.print_r" )

Command_Controller = {}
Command_Controller._index = Command_Controller

function Command_Controller.create(rules, game_state)
  local command_controller = {}
  
  --TO DO: Create a new Action Queue
  command_controller.action_queue = Queue.new()
  command_controller.game_state = game_state
  command_controller.game_rules = rules
  
  --currently assumes presorted commands
  command_controller.addCommand = function (command)
      command_controller.action_queue:add(command)
  end
  
  command_controller.processCommand = function ()
    local current_command = command_controller.action_queue:remove()
    
    if current_command == nil then
      --do nothing
    elseif command_controller.game_state == nil then
       print("No game state was provided.")
    elseif command_controller.game_rules == nil then
       print("No rules were provided to process commands.")
    else
      command_controller.game_rules.processCommand(current_command, game_state)
    end
     
    return current_command
  end
  
  command_controller.getGameState = function ()
    if command_controller.game_state == nil then
       print("There was no game state found.")
    else
      return command_controller.game_state
    end
  end
  
    
  return command_controller
end

return Command_Controller