local print_r = require( "common.print_r" )

Test_Rules = {}
Test_Rules._index = Test_Rules

function Test_Rules.create()
  local game_rules = {}
  
  --TO DO: Create a new Action Queue
  
  --currently assumes presorted commands
  game_rules.processCommand = function (command, game_state)
       print("Processing a command:")
       
       if command.command_type == "INCREMENT" then
        game_state.counter = game_state.counter + command.value
       else
        print("NOT FOUND")
       end
     
  end
  
  game_rules.createIncrementCommand = function (incValue)
    local command = {
      command_name = "TEST COMMAND",
      command_type = "INCREMENT",
      value = incValue
    }
    
    return command
  end
    
  
  game_rules.test_command = {
      command_name = "TEST COMMAND",
      command_type = "INCREMENT",
      value = 1
    }
  
    
  return game_rules
end

return Test_Rules