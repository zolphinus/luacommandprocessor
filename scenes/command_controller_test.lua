local composer = require( "composer" )
local print_r = require( "common.print_r" )
local test_rules = require( "classes.test_rules")

local command_controller = require( "classes.command_controller" )
 
local scene = composer.newScene()
 
-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------
 
 local player_party
 local enemy_party
 local battle
 
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
 
-- create()
function scene:create( event )
    local sceneGroup = self.view
    
    --Setup the battle
    
    local rules = test_rules.create()
    local game_state = {
      counter = 0
      }
    
    -- pass in rules and a state
    test_command_controller = command_controller.create(rules, game_state)
    print_r(test_command_controller.getGameState())
    
    
    
    
    test_command_controller.addCommand(rules.createIncrementCommand(4))
    test_command_controller.addCommand(rules.test_command)
    
    print("---------------------")
    test_command_controller.processCommand()
    test_command_controller.processCommand()
    test_command_controller.processCommand()
    
    print_r(test_command_controller.getGameState())
 
end
 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene