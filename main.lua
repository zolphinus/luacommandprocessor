local composer = require( "composer" )
local loadsave = require( "common.loadsave" )
local print_r = require( "common.print_r" )

--hide device status bar by default while game is loading/running
display.setStatusBar( display.HiddenStatusBar )

--global variables
settingsPath = "gameState.json"
gameState = loadsave.loadTable(settingsPath)

if gameState == nil then
	--No state found, create default game state
	gameState = {}
	
	loadsave.saveTable(gameSettings, settingsPath)
end

--grab seed on app load
math.randomseed( os.time() )


local options = { effect = "crossFade", time = 500, params = {} }

composer.gotoScene( "scenes.command_controller_test", options )